---
title: "Site Info"
# date: 2021-02-28T14:18:51-05:00
draft: false
---

Powered [by Hugo](https://gohugo.io/), a static site generator built using [the Go](https://golang.org/) programming language. The theme is custom and inspired by at least three existing Hugo themes.


## JavaScript Dependency

Initially, one of the main features of this custom theme is the use of the [DataTables jQuery plug-in](https://datatables.net/) to facilitate _SORTABLE, FILTERABLE_ tables in the post archive list pages. This almost makes separate list pages for various post taxonomies (e.g., categories, tags) moot since you can filter by taxa right in the post archive pages. When JavaScript is disabled, the tables will merely be rendered in static form, sorted by pinning weight, then publish date.

## Browser Support

No attempt is made to support obsolete web browsers. If you need a modern, standards-compliant, secure, privacy-oriented browser, give the [latest Firefox](https://www.mozilla.org/firefox/new/) a try.

## Dark Mode

For the time being, I recommend you simply toggle the [Firefox Reader View](https://support.mozilla.org/en-US/kb/firefox-reader-view-clutter-free-web-pages) or an analogous utility in another browser. I test post templates for this theme in Firefox Reader View.

## Reading Time Calculation

You may notice a different reading time if, for instance, you open one this site's posts in Firefox Reader View, which uses a different formula. <!-- By default, Hugo calculates estimated reading time using the following formula: -->

This site may opt to include multiple [calculation formulas](https://kodify.net/hugo/strings/reading-time-text/#discussion-calculate-a-strings-reading-time-in-hugo) at some point.