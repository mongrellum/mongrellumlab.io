---
# YAML front matter format; see https://gohugo.io/content-management/front-matter/
categories: [neptune] # separate terms with commas, quoting unnecessary
series: [] # separate terms with commas, quoting unnecessary
tags: [yellow] # separate terms with commas, quoting unnecessary
weight: 99999 # Uncomment and set a weight to make post pinned / sticky
toc: false # Controls if a table of contents should be generated for first-level links automatically.
title: "Bake Sale" # Title of the blog post.
description: "Article search engine description." # Description used for search engine.
# for timestamps, enable git options when multiple authors will edit on their own local file systems
lastmod: 2019-03-06T18:22:28Z # set this to override Hugo's own automatically generated lastmod timestamp
date: 2019-03-06T18:22:28Z # timestamp of post creation
publishDate: 2019-03-06T18:22:28Z # may optionally be later than actual creation (presumably a draft til "published")
draft: false # Sets whether to render this page. Draft of true will not be rendered.
type: posts
---

Cookies everywhere, OMG