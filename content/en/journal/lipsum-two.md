---
# YAML front matter format; see https://gohugo.io/content-management/front-matter/

categories: [] # separate terms with commas, quoting unnecessary
tags: [] # separate terms with commas, quoting unnecessary
series: [] # separate terms with commas, quoting unnecessary

# in post list page Datatables, cells with this default weight are hidden, and
# the "pinned" attribute is not set in the post single metadata template
weight: 99999

toc: false # Controls if a table of contents should be generated for first-level links automatically.

# type: posts # this DOES make it skip the desired single template

author: dg

title: "Lipsum Two" # Title of the blog post.
description: "Article search engine description." # Description used for search engine.

# for timestamps, enable git options when multiple authors will edit on their own local file systems
date: 2021-03-11T22:21:46-05:00 # timestamp of post creation
publishDate: 2021-03-11T22:21:46-05:00 # may optionally be later than actual creation (presumably a draft til "published")
# lastmod: 2021-03-11T22:21:46-05:00 # set this to override Hugo's own automatically generated lastmod timestamp
# expiryDate: 

draft: true # Sets whether to render this page. Draft of true will not be rendered.
---

Lorem