// copying URL to current post for sharing

function copyPostURLToClipboard(text) {
    var inputc = document.body.appendChild(document.createElement("input"));
    inputc.value = window.location.href;
    inputc.focus();
    inputc.select();
    document.execCommand('copy');
    inputc.parentNode.removeChild(inputc);
    alert("The following URL has been copied to the clipboard (you can now paste it wherever needed):\n"+ window.location.href);
}