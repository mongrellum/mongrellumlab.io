// initialize sortable datatables on posts list pages

$(document).ready(function() {
    $('.dynamic-post-archive').DataTable( {
        dom: 'Bfrtip',
        paging: false,
        "language": {
            "search": "Filter posts:"
        },
        lengthMenu: [
            [ 10, 25, 50, -1 ],
            [ '10 rows', '25 rows', '50 rows', 'Show all' ]
        ],
        buttons: [
            'pageLength'
        ]
    } );
} );
